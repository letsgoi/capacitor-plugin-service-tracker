# capacitor-plugin-service-tracker

Keep tracking event the app is closed. For packet transport needs.

## Install

```bash
npm install capacitor-plugin-service-tracker
npx cap sync
```

## API

<docgen-index>

* [`openLocationPermissionSettings()`](#openlocationpermissionsettings)
* [`checkBackgroundPermission()`](#checkbackgroundpermission)
* [Interfaces](#interfaces)
* [Type Aliases](#type-aliases)

</docgen-index>

<docgen-api>
<!--Update the source file JSDoc comments and rerun docgen to update the docs below-->

### openLocationPermissionSettings()

```typescript
openLocationPermissionSettings() => Promise<boolean>
```

**Returns:** <code>Promise&lt;boolean&gt;</code>

--------------------


### checkBackgroundPermission()

```typescript
checkBackgroundPermission() => Promise<PermissionState>
```

**Returns:** <code>Promise&lt;<a href="#permissionstate">PermissionState</a>&gt;</code>

--------------------


### Interfaces


#### PermissionState

| Prop           | Type                               |
| -------------- | ---------------------------------- |
| **`location`** | <code>'granted' \| 'denied'</code> |


### Type Aliases


#### PermissionState

<code>'prompt' | 'prompt-with-rationale' | 'granted' | 'denied'</code>

</docgen-api>

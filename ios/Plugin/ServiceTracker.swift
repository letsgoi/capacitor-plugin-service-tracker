import Foundation

@objc public class ServiceTracker: NSObject {
    @objc public func echo(_ value: String) -> String {
        print(value)
        return value
    }
}

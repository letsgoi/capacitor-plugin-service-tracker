package com.letsgoi.eyes.plugins.servicetracker

import android.Manifest
import android.content.pm.PackageManager
import android.util.Log
import androidx.core.app.ActivityCompat
import com.getcapacitor.*
import com.getcapacitor.annotation.CapacitorPlugin
import com.getcapacitor.annotation.Permission

const val LOCATION: String = "location"
const val COARSE_LOCATION: String = "coarseLocation"
const val TAG: String = "EYES-NATIVE"
const val FINE_LOCATION_PERM: String = Manifest.permission.ACCESS_FINE_LOCATION;
const val COARSE_LOCATION_PERM: String = Manifest.permission.ACCESS_COARSE_LOCATION

@CapacitorPlugin(
    name = "ServiceTracker",
    permissions = [
        Permission(
            strings = [FINE_LOCATION_PERM, COARSE_LOCATION_PERM],
            alias = LOCATION
        ),
        Permission(
            strings = [COARSE_LOCATION_PERM],
            alias = COARSE_LOCATION,
        )
    ]
)

class ServiceTrackerPlugin : Plugin() {
    @PluginMethod
    fun openLocationPermissionSettings(call: PluginCall) {
        try {
            gotoLocationSettings(call)
        } catch (err: Exception) {
            Log.e(TAG, "[err][native][android][openLocationPermissionSettings] $err")
            call.reject("[err][native][android][openLocationPermissionSettings] $err")
        }
    }

    @PluginMethod
    fun checkBackgroundPermission(call: PluginCall) {
       if (ActivityCompat.checkSelfPermission(
               context,
               Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED) {
           val ret: JSObject = JSObject()
           ret.put("location", "granted")
           call.resolve(ret)
       } else {
           val ret: JSObject = JSObject()
           ret.put("location", "denied")
           call.resolve(ret)
        }
    }

    private fun gotoLocationSettings(call: PluginCall) {
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, arrayOf<String>(Manifest.permission.ACCESS_BACKGROUND_LOCATION), 38784);
        } else {
            Log.e(TAG, "[err][native][android][onRequestAllPermissionResult] Location permission was denied")
            call.reject("[err][native][android][onRequestAllPermissionResult] Location permission was denied")
        }
    }
}
package com.letsgoi.eyes.plugins.servicetracker

import android.location.Location

interface LocationResultCallback {
    fun success(location: Location?)
    fun error(message: String?)
}
export interface ServiceTrackerPlugin {
  // getCurrentPosition(options: Options): Promise<Position>;
  openLocationPermissionSettings(): Promise<boolean>;
  checkBackgroundPermission(): Promise<PermissionState>;
}

export interface PermissionState {
  location: 'granted' | 'denied',
}
/*
export interface Options {
  timeout: number,
  maximumAge: number,
}

export interface Position {
  timestamp: number;
  coords: {
    latitude: number;
    longitude: number;
    accuracy: number;
    altitudeAccuracy: number | null | undefined;
    altitude: number | null;
    speed: number | null;
    heading: number | null;
  };
}
*/

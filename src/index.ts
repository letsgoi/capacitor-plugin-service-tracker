import { registerPlugin } from '@capacitor/core';

import type { ServiceTrackerPlugin } from './definitions';

const ServiceTracker = registerPlugin<ServiceTrackerPlugin>('ServiceTracker', {
  web: () => import('./web').then(m => new m.ServiceTrackerWeb()),
});

export * from './definitions';
export { ServiceTracker };

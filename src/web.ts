import { WebPlugin } from '@capacitor/core';

import type {
  ServiceTrackerPlugin,
  PermissionState,
  // Options,
  // Position,
} from './definitions';

export class ServiceTrackerWeb
  extends WebPlugin
  implements ServiceTrackerPlugin {
  /*
  async getCurrentPosition(options: Options): Promise<Position> {
    return Promise.reject(`Web Not implemented ${options}`);
  }
  */
  async openLocationPermissionSettings(): Promise<boolean> {
    return Promise.reject('Web Not implemented')
  }
  async checkBackgroundPermission(): Promise<PermissionState> {
    return Promise.reject('Web Not implemented')
  }
}
